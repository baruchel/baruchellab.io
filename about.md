---
title: About
permalink: /about/
---

This is the main blog of Thomas Baruchel — teacher in Philosophy and Computing Science.

If you follow blogs with an RSS aggregator, you should subscribe to my [feed](/feed.xml) with no concern about being too much disturbed since this blog will be a very low traffic site (only a few posts in a year).

An older blog, mostly devoted to functional programming in Python can be found at [http://baruchel.github.io/](http://baruchel.github.io/).

Some materials may be found in a personal directory hosted on [http://baruchel.metapath.org](http://baruchel.metapath.org).
